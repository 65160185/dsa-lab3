import java.util.Scanner;

public class lab301 {
    public static int sqrt (int X){
        if(X==0){
             return 0;
        }
        int low = 0;
        int high = X;
        while (low <= high) {
            int mid = (low + high)/2;
            if (mid * mid <= X && X < (mid+1) * (mid+1)) {
                 return mid;
            }else if (X<mid*mid) {
                high = mid + 1;
            }else{
                low = mid + 1;
            }
        }
         return high;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int X;
        System.out.print("Input: x = ");
        X = sc.nextInt();
        System.out.println(sqrt(X));
    }
}
